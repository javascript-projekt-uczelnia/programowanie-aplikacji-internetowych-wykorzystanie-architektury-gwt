package com.example.client.views.content;

import com.example.client.SessionDataManager;
import com.example.client.presenters.PresentersManager;
import com.google.gwt.user.client.ui.FlowPanel;

public class MainContentView extends FlowPanel {
    public MainContentView() {
        getElement().addClassName(
                "column auto is-flex is-justify-content-center is-align-items-center p-0 m-0 has-background-black-bis");
    }

    public void refresh(SessionDataManager.MainContentView view) {
        clear();

        switch (view) {
            case LOGIN:
                add(PresentersManager.getLoginPresenter().getView());
                break;

            case REGISTER:
                add(PresentersManager.getRegisterPresenter().getView());
                break;

            default:
                add(PresentersManager.getTaskContentPresenter().getView());
                break;
        }
    }
}
