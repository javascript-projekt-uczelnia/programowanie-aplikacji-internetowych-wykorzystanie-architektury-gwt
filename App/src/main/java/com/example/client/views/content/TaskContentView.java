package com.example.client.views.content;

import java.util.List;

import com.example.client.SessionDataManager;
import com.example.client.components.NewTaskForm;
import com.example.client.components.tasks.AwaitingForAdminTaskPane;
import com.example.client.components.tasks.AwaitingForUserTaskPane;
import com.example.client.components.tasks.FinishedTaskPane;
import com.example.client.components.tasks.InProgressTaskPane;
import com.example.client.presenters.PresentersManager;
import com.example.shared.dto.TaskDTO;
import com.example.shared.dto.UserDTO;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ScrollPanel;

public class TaskContentView extends FlowPanel {
    private final ScrollPanel scrollTasksPanel;
    private final FlowPanel taksContainer;

    private final FlowPanel newTaskContainer;

    public TaskContentView() {
        setHeight("100%");
        setWidth("100%");

        getElement().addClassName("is-flex is-flex-direction-column columns");

        scrollTasksPanel = new ScrollPanel();
        scrollTasksPanel.getElement().addClassName("column my-3");

        taksContainer = new FlowPanel();
        taksContainer.getElement().addClassName("columns is-multiline");

        scrollTasksPanel.add(taksContainer);

        newTaskContainer = new FlowPanel();
        newTaskContainer.getElement().addClassName("column is-narrow is-flex is-justify-content-center mb-3");
        newTaskContainer.add(new NewTaskForm());

        add(scrollTasksPanel);
    }

    public void refresh(TaskDTO.State taskContentView) {
        UserDTO user = SessionDataManager.getUser();
        if (taskContentView == TaskDTO.State.AWAITING_FOR_USER
                && user != null && user.isAdmin()) {
            add(newTaskContainer);
        } else {
            remove(newTaskContainer);
        }

        taksContainer.clear();
        List<TaskDTO> tasks = SessionDataManager.getTasks();

        if (tasks == null) {
            return;
        }

        int awaitingForUserCount = 0;
        int inProgressCount = 0;
        int awaitingForAdminCount = 0;
        int finishedCount = 0;

        for (TaskDTO task : tasks) {
            TaskDTO.State state = task.getState();

            switch (state) {
                case AWAITING_FOR_USER:
                    ++awaitingForUserCount;
                    if (state == taskContentView) {
                        taksContainer.add(new AwaitingForUserTaskPane(task));
                    }
                    break;

                case IN_PROGRESS:
                    ++inProgressCount;
                    if (state == taskContentView) {
                        taksContainer.add(new InProgressTaskPane(task));
                    }
                    break;

                case AWAITING_FOR_ADMIN:
                    ++awaitingForAdminCount;
                    if (state == taskContentView) {
                        taksContainer.add(new AwaitingForAdminTaskPane(task));
                    }
                    break;

                case FINISHED:
                    ++finishedCount;
                    if (state == taskContentView) {
                        taksContainer.add(new FinishedTaskPane(task));
                    }
                    break;
            }
        }

        PresentersManager.getNavListPresenter().getView().updateTasksCount(awaitingForUserCount, inProgressCount,
                awaitingForAdminCount, finishedCount);
    }
}
