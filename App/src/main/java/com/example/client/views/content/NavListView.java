package com.example.client.views.content;

import com.example.client.SessionDataManager;
import com.example.client.components.IconButton;
import com.example.shared.dto.UserDTO;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class NavListView extends VerticalPanel {
    public IconButton accountBtn;
    public IconButton logOutBtn;
    public IconButton awaitingForUserBtn;
    public IconButton inProgressBtn;
    public IconButton awaitingForAdminBtn;
    public IconButton finishedBtn;
    public IconButton logInBtn;
    public IconButton registerBtn;

    public NavListView() {
        getElement().addClassName("column is-narrow m-0 p-0 has-background-black-ter pt-3 px-3");
    }

    public void refresh() {
        clear();
        createLabel("Konto");

        UserDTO user = SessionDataManager.getUser();
        if (user != null) {
            accountBtn = createButton(
                    user.getLogin() + (user.isAdmin() ? " (Admin)" : " (Użytkownik)"),
                    "fa-user");
            logOutBtn = createButton("Wyloguj", "fa-sign-out-alt");

            Label label = createLabel("Zadania");
            label.setStyleName(label.getStyleName() + " mt-2");

            awaitingForUserBtn = createButton("Oczekujące", "fa-plus");
            awaitingForUserBtn.addClickHandler(event -> {

            });

            inProgressBtn = createButton("W trakcie", "fa-bars-progress");
            inProgressBtn.addClickHandler(event -> {

            });

            awaitingForAdminBtn = createButton("Do sprawdzenia", "fa-circle-half-stroke");
            awaitingForAdminBtn.addClickHandler(event -> {

            });

            finishedBtn = createButton("Ukończone", "fa-box-archive");
            finishedBtn.addClickHandler(event -> {

            });
        } else {
            logInBtn = createButton("Logowanie", "fa-sign-in");
            logInBtn.addClickHandler(event -> {

            });

            registerBtn = createButton("Rejestracja", "fa-user-plus");
            registerBtn.addClickHandler(event -> {

            });
        }
    }

    private Label createLabel(String text) {
        Label label = new Label(text);
        label.setStyleName("has-text-white is-size-5");
        add(label);

        return label;
    }

    private IconButton createButton(String text, String icon) {
        IconButton button = new IconButton(text, icon);
        add(button);

        return button;
    }

    public void updateTasksCount(int awaitingForUserCount, int inProgressCount, int awaitingForAdminCount,
            int finishedCount) {
        awaitingForUserBtn.setText("Oczekujące (" + awaitingForUserCount + ")");
        inProgressBtn.setText("W trakcie (" + inProgressCount + ")");
        awaitingForAdminBtn.setText("Do sprawdzenia (" + awaitingForAdminCount + ")");
        finishedBtn.setText("Ukończone (" + finishedCount + ")");
    }
}
