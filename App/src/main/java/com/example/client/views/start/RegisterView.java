package com.example.client.views.start;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;

public class RegisterView extends FlowPanel {
    public Button btn;
    public TextBox logInTextBox;
    public PasswordTextBox passwordTextBox;
    public PasswordTextBox repeatPasswordTextBox;

    public RegisterView() {
        getElement().addClassName(
                "box has-background-grey-darker px-6 py-0");

        Label header = new Label("Rejestracja");
        header.getElement().addClassName("has-text-white my-4 is-size-3 mt-6 has-text-centered is-family-monospace");

        // Login:
        FlowPanel loginField = new FlowPanel();
        loginField.getElement().addClassName("field");

        FlowPanel loginControl = new FlowPanel();
        loginControl.getElement().addClassName("control");

        logInTextBox = new TextBox();
        logInTextBox.getElement().setAttribute("placeholder", "Wprowadź login");
        logInTextBox.getElement().addClassName("input is-medium my-3");

        loginControl.add(logInTextBox);
        loginField.add(loginControl);

        // Password:
        FlowPanel passwordField = new FlowPanel();
        passwordField.getElement().addClassName("field");

        FlowPanel passwordControl = new FlowPanel();
        passwordControl.getElement().addClassName("control");

        passwordTextBox = new PasswordTextBox();
        passwordTextBox.getElement().setAttribute("placeholder", "Wprowadź hasło");
        passwordTextBox.getElement().addClassName("input is-medium my-3");

        passwordControl.add(passwordTextBox);
        passwordField.add(passwordControl);

        // Repeat password:
        FlowPanel repeatPasswordField = new FlowPanel();
        repeatPasswordField.getElement().addClassName("field");

        FlowPanel repeatPasswordControl = new FlowPanel();
        repeatPasswordControl.getElement().addClassName("control");

        repeatPasswordTextBox = new PasswordTextBox();
        repeatPasswordTextBox.getElement().setAttribute("placeholder", "Powtórz hasło");
        repeatPasswordTextBox.getElement().addClassName("input is-medium my-3");

        repeatPasswordControl.add(repeatPasswordTextBox);
        repeatPasswordField.add(repeatPasswordControl);

        // Button:
        FlowPanel btnField = new FlowPanel();
        btnField.getElement().addClassName("field");

        FlowPanel btnControl = new FlowPanel();
        btnControl.getElement().addClassName("control is-flex is-justify-content-center");

        btn = new Button("Zarejestru się");
        btn.getElement().addClassName("button has-background-primary-dark has-text-white my-6 is-medium");

        btnControl.add(btn);
        btnField.add(btnControl);

        add(header);
        add(loginField);
        add(passwordField);
        add(repeatPasswordField);
        add(btnField);
    }
}