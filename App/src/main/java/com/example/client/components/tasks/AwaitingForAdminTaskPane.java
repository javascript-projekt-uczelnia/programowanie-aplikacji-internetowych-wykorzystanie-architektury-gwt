package com.example.client.components.tasks;

import com.example.client.SessionDataManager;
import com.example.shared.dto.TaskDTO;
import com.example.shared.dto.UserDTO;
import com.google.gwt.user.client.ui.Button;

public class AwaitingForAdminTaskPane extends TaskPane {

    public AwaitingForAdminTaskPane(TaskDTO task) {
        super(task);

        UserDTO user = SessionDataManager.getUser();
        Boolean isAdmin = user.isAdmin();

        if (isAdmin) {
            Button returnButton = createButton("Cofnij");
            bindChangingState(returnButton, TaskDTO.State.IN_PROGRESS);

            Button acceptButton = createButton("Zakończ");
            bindChangingState(acceptButton, TaskDTO.State.FINISHED);

        } else if (user.getLogin().equals(task.getAssignee().getLogin())) {
            Button returnButton = createButton("Cofnij");
            bindChangingState(returnButton, TaskDTO.State.IN_PROGRESS);
        }
    }
}
