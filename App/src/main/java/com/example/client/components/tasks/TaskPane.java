package com.example.client.components.tasks;

import java.util.List;

import com.example.client.SessionDataManager;
import com.example.client.Utils;
import com.example.shared.dto.TaskDTO;
import com.example.shared.services.TaskService;
import com.example.shared.services.TaskServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

public class TaskPane extends FlowPanel {
    private final FlowPanel container;
    private final FlowPanel buttonsContainer;
    private final TaskDTO task;

    private static final TaskServiceAsync taskService = GWT.create(TaskService.class);

    public TaskPane(TaskDTO task) {
        this.task = task;
        addStyleName("column py-3 px-4 is-half");

        container = new FlowPanel();
        container
                .addStyleName("is-flex is-flex-direction-column box has-background-grey-darker has-text-white m-0 p-3");

        Label titleLabel = createLabel(task.getId() + ". " + task.getTitle());
        titleLabel.getElement().addClassName("has-text-weight-bold");

        createLabel("Opis: " + task.getDescription());
        createLabel("Termin: " + Utils.getDateText(task.getDeadlineDate()));

        String assigneeName = (task.getAssignee() != null) ? task.getAssignee().getLogin() : "Brak";
        createLabel("Przypisano: " + assigneeName);

        buttonsContainer = new FlowPanel();
        buttonsContainer.getElement().setClassName("mt-3");

        Button detailsButton = createButton("Pokaż szczegóły");
        detailsButton.addClickHandler(event -> {
            DetailsPane detailsPane = new DetailsPane(task);
            detailsPane.center();
            detailsPane.show();
        });

        add(container);
        container.add(buttonsContainer);
    }

    protected Button createButton(String text) {
        Button button = new Button(text);
        button.addStyleName("button is-small has-background-primary-dark has-text-white mr-3");

        buttonsContainer.add(button);
        return button;
    }

    private Label createLabel(String text) {
        Label label = new Label(text);

        label.setWordWrap(true);
        container.add(label);

        return label;
    }

    protected void bindChangingAssignee(Button btn) {
        btn.addClickHandler(event -> {
            taskService.setAssignee(task.getId(),
                    new AsyncCallback<List<TaskDTO>>() {
                        @Override
                        public void onSuccess(List<TaskDTO> tasks) {
                            if (tasks != null) {
                                SessionDataManager.setTasks(tasks);
                            } else {
                                Window.alert("Wystąpił błąd!");
                            }
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                        }
                    });
        });
    }

    protected void bindChangingState(Button btn, TaskDTO.State state) {
        btn.addClickHandler(event -> {
            taskService.setState(task.getId(), state.getValue(),
                    new AsyncCallback<List<TaskDTO>>() {
                        @Override
                        public void onSuccess(List<TaskDTO> tasks) {
                            if (tasks != null) {
                                SessionDataManager.setTasks(tasks);
                            } else {
                                Window.alert("Wystąpił błąd!");
                            }
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                        }
                    });
        });
    }
}