package com.example.client.components.tasks;

import com.example.client.SessionDataManager;
import com.example.shared.dto.TaskDTO;
import com.google.gwt.user.client.ui.Button;

public class FinishedTaskPane extends TaskPane {
    public FinishedTaskPane(TaskDTO task) {
        super(task);

        if (SessionDataManager.getUser().isAdmin()) {
            Button returnButton = createButton("Cofnij");
            bindChangingState(returnButton, TaskDTO.State.AWAITING_FOR_ADMIN);
        }
    }
}
