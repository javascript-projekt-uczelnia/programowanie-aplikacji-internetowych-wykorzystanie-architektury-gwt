package com.example.client.components.tasks;

import com.example.client.SessionDataManager;
import com.example.shared.dto.TaskDTO;
import com.example.shared.dto.UserDTO;
import com.google.gwt.user.client.ui.Button;

public class InProgressTaskPane extends TaskPane {
    public InProgressTaskPane(TaskDTO task) {
        super(task);

        UserDTO user = SessionDataManager.getUser();
        if (user.isAdmin() || task.getAssignee().getLogin().equals(user.getLogin())) {
            Button leaveButton = createButton("Zwolnij");
            bindChangingAssignee(leaveButton);

            Button readyButton = createButton("Do sprawdzenia");
            bindChangingState(readyButton, TaskDTO.State.AWAITING_FOR_ADMIN);
        }
    }
}
