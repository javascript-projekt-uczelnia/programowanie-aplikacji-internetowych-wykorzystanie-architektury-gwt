package com.example.client.components;

import java.util.Date;
import java.util.List;

import com.example.client.SessionDataManager;
import com.example.client.Utils;
import com.example.shared.services.TaskService;
import com.example.shared.services.TaskServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.example.shared.dto.TaskDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class NewTaskForm extends FlowPanel {
    private final TaskServiceAsync taskService = GWT.create(TaskService.class);

    public TextBox titleTextBox;
    public TextArea contentTextArea;
    public TextBox datePicker;
    public Button btn;

    public NewTaskForm() {
        createView();

        btn.addClickHandler(event -> {
            String title = titleTextBox.getText();
            if (title.length() == 0 || title.trim().isEmpty()) {
                Window.alert("Nie podano tytułu!");
                return;
            }

            String description = contentTextArea.getText();
            if (description.length() == 0 || description.trim().isEmpty()) {
                Window.alert("Nie podano opisu!");
                return;
            }

            if (description.length() > 1000) {
                description = description.substring(0, 1000);
            }

            Date date = Utils.getDate(datePicker.getValue());
            if (date == null) {
                Window.alert("Nie podano poprawnego terminu!");
                return;
            }

            taskService.createTask(title, description, date,
                    new AsyncCallback<List<TaskDTO>>() {
                        @Override
                        public void onSuccess(List<TaskDTO> tasks) {
                            Window.alert("Dodano nowe zadanie!");
                            SessionDataManager.setTasks(tasks);
                            titleTextBox.setText("");
                            contentTextArea.setText("");
                            datePicker.setText("");
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                            Window.alert("Wystąpił błąd!");
                        }
                    });
        });
    }

    public void createView() {
        getElement().addClassName(
                "box has-background-grey-darker px-6 py-0");

        Label header = new Label("Dodaj nowe zadanie");
        header.getElement()
                .addClassName("has-text-white mt-3 is-size-4 has-text-centered is-family-monospace");

        // Title:
        FlowPanel titleField = new FlowPanel();
        titleField.getElement().addClassName("field");

        FlowPanel titleControl = new FlowPanel();
        titleControl.getElement().addClassName("control");

        titleTextBox = new TextBox();
        titleTextBox.getElement().setAttribute("placeholder", "Wprowadź tytuł");
        titleTextBox.getElement().addClassName("input mt-3");

        titleControl.add(titleTextBox);
        titleField.add(titleControl);

        FlowPanel contentField = new FlowPanel();
        contentField.getElement().addClassName("field");

        FlowPanel contentControl = new FlowPanel();
        contentControl.getElement().addClassName("control");

        contentTextArea = new TextArea();
        contentTextArea.getElement().setAttribute("placeholder", "Wprowadź opis");
        contentTextArea.getElement().addClassName("textarea mt-3");

        contentControl.add(contentTextArea);
        contentField.add(contentControl);

        // Date:
        FlowPanel dateField = new FlowPanel();
        dateField.getElement().addClassName("field");

        FlowPanel dateControl = new FlowPanel();
        dateControl.getElement().addClassName("control");

        datePicker = new TextBox();
        datePicker.getElement().addClassName("input mt-3 is-flex");
        datePicker.getElement().setAttribute("type", "datetime-local");

        dateControl.add(datePicker);
        dateField.add(dateControl);

        // Button:
        FlowPanel btnField = new FlowPanel();
        btnField.getElement().addClassName("field");

        FlowPanel btnControl = new FlowPanel();
        btnControl.getElement().addClassName("control is-flex is-justify-content-center");

        btn = new Button("Potwierdź");
        btn.getElement().addClassName("button has-background-primary-dark has-text-white mb-3");

        btnControl.add(btn);
        btnField.add(btnControl);

        add(header);
        add(titleField);
        add(contentField);
        add(dateField);
        add(btnField);
    }
}