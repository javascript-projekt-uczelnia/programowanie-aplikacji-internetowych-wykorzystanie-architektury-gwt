package com.example.client.components.tasks;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.*;

import java.util.List;

import com.example.client.Utils;
import com.example.shared.dto.TaskDTO;
import com.example.shared.dto.TasksCommentDTO;
import com.example.shared.services.CommentService;
import com.example.shared.services.CommentServiceAsync;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class DetailsPane extends DialogBox {
    private final TaskDTO task;
    private FlowPanel mainContainer;
    private FlowPanel textContainer;
    private FlowPanel commentsContainer;

    private static final CommentServiceAsync commentService = GWT.create(CommentService.class);

    public DetailsPane(TaskDTO task) {
        this.task = task;

        setModal(true);
        setAnimationEnabled(true);
        addStyleName("container is-max-desktop");
        center();

        mainContainer = new FlowPanel();
        mainContainer.addStyleName(
                "is-flex is-flex-direction-column box has-background-black-ter has-text-white p-3 pb-0 m-0");

        FlowPanel closeButtonTopPanel = new FlowPanel();
        closeButtonTopPanel.addStyleName("is-flex is-justify-content-end");

        Button closeButtonTop = new Button("X");
        closeButtonTop.addStyleName("delete has-background-grey-lighter mb-3");

        closeButtonTop.addClickHandler(event -> hide());
        closeButtonTopPanel.add(closeButtonTop);

        mainContainer.add(closeButtonTopPanel);

        createTextContainer();
        createAddCommentContainer();
        createCommentsContainer();
        fetchComments();

        setWidget(mainContainer);
    }

    private Label createLabel(String text) {
        Label label = new Label(text);

        label.setWordWrap(true);
        label.getElement().addClassName("mb-1 mr-6");
        textContainer.add(label);

        return label;
    }

    private void createTextContainer() {
        textContainer = new FlowPanel();
        textContainer.addStyleName(
                "is-flex is-flex-direction-column");

        Label titleLabel = createLabel(task.getId() + ". " + task.getTitle());
        titleLabel.getElement().addClassName("has-text-weight-bold");

        createLabel("Utworzono: " + Utils.getDateText(task.getCreationDate()));
        createLabel("Termin: " + Utils.getDateText(task.getDeadlineDate()));
        String stateName = "";
        switch (task.getState()) {
            case AWAITING_FOR_USER:
                stateName = "Oczekujące";
                break;

            case IN_PROGRESS:
                stateName = "W trakcie";
                break;

            case AWAITING_FOR_ADMIN:
                stateName = "Do sprawdzenia";
                break;

            case FINISHED:
                stateName = "Ukończone";
                break;

            default:
                break;
        }

        createLabel("Status: " + stateName);

        String assigneeName = (task.getAssignee() != null) ? task.getAssignee().getLogin() : "Brak";
        createLabel("Przypisano: " + assigneeName);

        createLabel("Opis: " + task.getDescription());

        ScrollPanel scrollTextContainer = new ScrollPanel();
        scrollTextContainer.getElement().addClassName("scrollableContent300px");
        scrollTextContainer.add(textContainer);

        mainContainer.add(scrollTextContainer);
    }

    private void createAddCommentContainer() {
        FlowPanel container = new FlowPanel();
        container.addStyleName(
                "is-flex is-flex-direction-column");

        FlowPanel commentField = new FlowPanel();
        commentField.getElement().addClassName("field mt-3");

        FlowPanel commentControl = new FlowPanel();
        commentControl.getElement().addClassName("control");

        TextArea commentTextArea = new TextArea();
        commentTextArea.getElement().setAttribute("placeholder", "Wprowadź komentarz");
        commentTextArea.getElement().addClassName("textarea p-1");

        commentControl.add(commentTextArea);
        commentField.add(commentControl);

        container.add(commentField);

        FlowPanel addCommentBtnPanel = new FlowPanel();
        addCommentBtnPanel.addStyleName(
                "is-flex is-justify-content-right mb-3");

        Button addCommentBtn = new Button("Dodaj");
        addCommentBtn.addStyleName(
                "button has-background-primary-dark has-text-white");
        addCommentBtn.addClickHandler(event -> {
            String content = commentTextArea.getText();
            if (content.length() == 0 || content.trim().isEmpty()) {
                Window.alert("Nie podano treści!");
                return;
            }

            if (content.length() > 1000) {
                content = content.substring(0, 1000);
            }

            commentService.createComment(task.getId(), content, new AsyncCallback<List<TasksCommentDTO>>() {
                @Override
                public void onSuccess(List<TasksCommentDTO> comments) {
                    if (comments != null) {
                        commentTextArea.setText("");
                        refreshComments(comments);
                    } else {
                        Window.alert("Wystąpił błąd!");
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    Window.alert("Wystąpił błąd!");
                }
            });
        });

        addCommentBtnPanel.add(addCommentBtn);
        container.add(addCommentBtnPanel);

        mainContainer.add(container);
    }

    private void createCommentsContainer() {
        ScrollPanel scrollPanel = new ScrollPanel();
        scrollPanel.addStyleName(
                "scrollableContent300px");

        commentsContainer = new FlowPanel();
        commentsContainer.addStyleName(
                "is-flex is-flex-direction-column");

        scrollPanel.add(commentsContainer);
        mainContainer.add(scrollPanel);
    }

    private void refreshComments(List<TasksCommentDTO> comments) {
        commentsContainer.clear();
        for (TasksCommentDTO comment : comments) {
            FlowPanel panel = new FlowPanel();
            panel.addStyleName(
                    "is-flex is-flex-direction-column box has-background-grey-darker has-text-white m-0 mt-3 p-3");

            panel.add(new Label("Dodano: " + Utils.getDateText(comment.getCreationDate()) + " przez "
                    + comment.getUser().getLogin()));
            panel.add(new Label(comment.getContent()));

            commentsContainer.add(panel);
        }

        center();
    }

    private void fetchComments() {
        commentService.getAllComments(task.getId(), new AsyncCallback<List<TasksCommentDTO>>() {
            @Override
            public void onSuccess(List<TasksCommentDTO> comments) {
                if (comments != null) {
                    refreshComments(comments);
                } else {
                    commentsContainer.clear();
                }
            }

            @Override
            public void onFailure(Throwable caught) {
            }
        });
    }
}
