package com.example.client.components;

import com.google.gwt.user.client.ui.Button;

public class IconButton extends Button {

    private String iconHtml;
    private String textHtml;

    public IconButton(String text, String icon) {
        super();
        this.setStyleName("button is-ghost has-text-white is-size-6");
        this.textHtml = text != null ? text : "";
        this.iconHtml = "<span class=\"icon\"><i class=\"fas " + icon + " mr-2\"></i></span>";
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        getElement().setInnerHTML(iconHtml + "<span>" + textHtml + "</span>");
    }
}
