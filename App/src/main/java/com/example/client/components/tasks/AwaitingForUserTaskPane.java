package com.example.client.components.tasks;

import com.example.shared.dto.TaskDTO;
import com.google.gwt.user.client.ui.Button;

public class AwaitingForUserTaskPane extends TaskPane {
    public AwaitingForUserTaskPane(TaskDTO task) {
        super(task);

        Button assignButton = createButton("Przejmij");
        bindChangingAssignee(assignButton);
    }
}