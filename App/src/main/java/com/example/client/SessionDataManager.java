package com.example.client;

import java.util.List;

import com.example.client.presenters.PresentersManager;
import com.example.shared.dto.TaskDTO;
import com.example.shared.dto.UserDTO;
import com.example.shared.services.TaskService;
import com.example.shared.services.TaskServiceAsync;
import com.example.shared.services.UserService;
import com.example.shared.services.UserServiceAsync;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.core.client.GWT;

public class SessionDataManager {
    private static final UserServiceAsync userService = GWT.create(UserService.class);
    private static final TaskServiceAsync taskService = GWT.create(TaskService.class);

    public static enum MainContentView {
        LOGIN,
        REGISTER,
        TASKS
    }

    private static MainContentView mainContentView = MainContentView.LOGIN;
    private static Storage storage = Storage.getSessionStorageIfSupported();
    private static TaskDTO.State taskContentView = TaskDTO.State.AWAITING_FOR_USER;
    private static UserDTO user = null;

    private static List<TaskDTO> tasks;

    public static MainContentView getMainContentView() {
        return mainContentView;
    }

    public static void setMainContentView(MainContentView mainContentView) {
        SessionDataManager.mainContentView = mainContentView;
        storage.setItem("mainContentView", mainContentView.toString());
    }

    public static TaskDTO.State getTaskContentView() {
        return taskContentView;
    }

    public static void setTaskContentView(TaskDTO.State taskContentView) {
        SessionDataManager.taskContentView = taskContentView;
        storage.setItem("taskContentView", taskContentView.toString());
    }

    public static void init() {
        try {
            SessionDataManager
                    .setMainContentView(MainContentView.valueOf(storage.getItem("mainContentView")));
        } catch (Exception e) {
            setMainContentView(MainContentView.LOGIN);
        }

        userService.whoAmI(
                new AsyncCallback<UserDTO>() {
                    @Override
                    public void onSuccess(UserDTO user) {
                        setUser(user);
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                    }
                });

        try {
            taskContentView = TaskDTO.State.valueOf(storage.getItem("taskContentView"));
        } catch (Exception e) {
            taskContentView = TaskDTO.State.AWAITING_FOR_USER;
        }
    }

    public static UserDTO getUser() {
        return user;
    }

    public static void setUser(UserDTO user) {
        if (user != null) {
            SessionDataManager.user = user;
            setMainContentView(MainContentView.TASKS);
            fetchTasks();
        } else {
            resetUser();
        }

        PresentersManager.getNavListPresenter().refreshView();
        PresentersManager.getMainContentPresenter().refreshView();
    }

    private static void resetUser() {
        user = null;
        if (mainContentView == MainContentView.TASKS) {
            setMainContentView(MainContentView.LOGIN);
        }
    }

    private static void fetchTasks() {
        taskService.getAllTasks(
                new AsyncCallback<List<TaskDTO>>() {
                    @Override
                    public void onSuccess(List<TaskDTO> tasks) {
                        setTasks(tasks);
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                    }
                });
    }

    public static List<TaskDTO> getTasks() {
        return tasks;
    }

    public static void setTasks(List<TaskDTO> tasks) {
        SessionDataManager.tasks = tasks;
        PresentersManager.getTaskContentPresenter().refreshView();
    }
}
