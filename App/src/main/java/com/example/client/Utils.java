package com.example.client;

import com.google.gwt.i18n.client.DateTimeFormat;
import java.util.Date;

public class Utils {
    private static DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy HH:mm");
    private static DateTimeFormat inputDateFormat = DateTimeFormat.getFormat("yyyy-MM-dd'T'HH:mm");

    public static String getDateText(Date date) {
        return dateFormat.format(date);
    }

    public static Date getDate(String dateString) {
        if (dateString.length() == 0 || dateString.trim().isEmpty()) {
            return null;
        }

        Date result = null;
        try {
            result = inputDateFormat.parse(dateString);
        } catch (Exception e) {
        }

        return result;
    }
}