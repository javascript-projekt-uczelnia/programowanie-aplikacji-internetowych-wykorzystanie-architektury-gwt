package com.example.client.presenters;

import com.example.client.presenters.content.MainContentPresenter;
import com.example.client.presenters.content.NavListPresenter;
import com.example.client.presenters.content.TaskContentPresenter;
import com.example.client.presenters.start.LogInPresenter;
import com.example.client.presenters.start.RegisterPresenter;
import com.example.client.views.content.MainContentView;
import com.example.client.views.content.NavListView;
import com.example.client.views.content.TaskContentView;
import com.example.client.views.start.LoginView;
import com.example.client.views.start.RegisterView;

public class PresentersManager {
    private static LogInPresenter loginPresenter;
    private static RegisterPresenter registerPresenter;

    private static TaskContentPresenter taskContentPresenter;

    private static NavListPresenter navListPresenter;
    private static MainContentPresenter mainContentPresenter;

    public static void init() {
        loginPresenter = new LogInPresenter(new LoginView());
        registerPresenter = new RegisterPresenter(new RegisterView());

        taskContentPresenter = new TaskContentPresenter(new TaskContentView());

        navListPresenter = new NavListPresenter(new NavListView());
        mainContentPresenter = new MainContentPresenter(new MainContentView());
    }

    public static LogInPresenter getLoginPresenter() {
        return loginPresenter;
    }

    public static RegisterPresenter getRegisterPresenter() {
        return registerPresenter;
    }

    public static TaskContentPresenter getTaskContentPresenter() {
        return taskContentPresenter;
    }

    public static NavListPresenter getNavListPresenter() {
        return navListPresenter;
    }

    public static MainContentPresenter getMainContentPresenter() {
        return mainContentPresenter;
    }
}
