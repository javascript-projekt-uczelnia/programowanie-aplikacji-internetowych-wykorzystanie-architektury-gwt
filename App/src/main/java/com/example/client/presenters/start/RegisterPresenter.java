package com.example.client.presenters.start;

import com.example.client.SessionDataManager;
import com.example.client.views.start.RegisterView;
import com.example.shared.dto.UserDTO;
import com.example.shared.services.UserService;
import com.example.shared.services.UserServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class RegisterPresenter {
    private final RegisterView view;
    private final UserServiceAsync userService = GWT.create(UserService.class);

    public RegisterPresenter(RegisterView view) {
        this.view = view;
        bind();
    }

    private void bind() {
        view.btn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String login = view.logInTextBox.getText();
                String password = view.passwordTextBox.getText();
                String repeatedPassword = view.repeatPasswordTextBox.getText();

                if (login.length() == 0 || login.trim().isEmpty()) {
                    Window.alert("Nie podano loginu!");
                    return;
                }

                if (password.length() == 0 || password.trim().isEmpty()) {
                    Window.alert("Nie podano hasła!");
                    return;
                }

                if (repeatedPassword.length() == 0 || repeatedPassword.trim().isEmpty()) {
                    Window.alert("Nie powtórzono hasła!");
                    return;
                }

                if (!password.equals(repeatedPassword)) {
                    Window.alert("Błędnie powtórzono hasło!");
                    return;
                }

                if (password.length() == 0 || password.trim().isEmpty()) {
                    Window.alert("Nie podano hasła!");
                    return;
                }

                userService.registerIn(login, password, new AsyncCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean isCreated) {
                        if (!isCreated) {
                            Window.alert("Podany login jest już zajęty!");
                            return;
                        }

                        view.logInTextBox.setText("");
                        view.passwordTextBox.setText("");
                        view.repeatPasswordTextBox.setText("");

                        Window.alert("Utworzono nowe konto!");

                        userService.logIn(login, password, new AsyncCallback<UserDTO>() {
                            @Override
                            public void onSuccess(UserDTO user) {
                                SessionDataManager.setUser(user);
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                                Window.alert("Wystąpił błąd!");
                            }
                        });
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert("Wystąpił błąd!");
                    }
                });
            }
        });
    }

    public RegisterView getView() {
        return view;
    }
}