package com.example.client.presenters.content;

import com.example.client.SessionDataManager;
import com.example.client.views.content.TaskContentView;
import com.example.shared.dto.TaskDTO;

public class TaskContentPresenter {
    private final TaskContentView view;

    public TaskContentPresenter(TaskContentView view) {
        this.view = view;
        bind();
    }

    public void bind() {

    }

    public void refreshView() {
        view.refresh(SessionDataManager.getTaskContentView());
    }

    public TaskContentView getView() {
        return view;
    }

    public void toggleView(TaskDTO.State view) {
        if (SessionDataManager.getTaskContentView() == view) {
            return;
        }

        SessionDataManager.setTaskContentView(view);
        refreshView();
    }
}
