package com.example.client.presenters.start;

import com.example.client.SessionDataManager;
import com.example.client.views.start.LoginView;
import com.example.shared.dto.UserDTO;
import com.example.shared.services.UserService;
import com.example.shared.services.UserServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class LogInPresenter {
    private final LoginView view;
    private final UserServiceAsync userService = GWT.create(UserService.class);

    public LogInPresenter(LoginView view) {
        this.view = view;
        bind();
    }

    private void bind() {
        view.btn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String login = view.logInTextBox.getText();
                String password = view.passwordTextBox.getText();

                if (login.length() == 0 || login.trim().isEmpty()) {
                    Window.alert("Nie podano loginu!");
                    return;
                }

                if (password.length() == 0 || password.trim().isEmpty()) {
                    Window.alert("Nie podano hasła!");
                    return;
                }

                view.logInTextBox.setText("");
                view.passwordTextBox.setText("");

                userService.logIn(login, password, new AsyncCallback<UserDTO>() {
                    @Override
                    public void onSuccess(UserDTO user) {
                        if (user == null) {
                            Window.alert("Błędy login lub hasło!");
                            return;
                        }

                        SessionDataManager.setUser(user);
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert("Wystąpił błąd!");
                    }
                });
            }
        });
    }

    public LoginView getView() {
        return view;
    }
}