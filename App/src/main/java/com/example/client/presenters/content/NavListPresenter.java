package com.example.client.presenters.content;

import com.example.client.SessionDataManager;
import com.example.client.presenters.PresentersManager;
import com.example.client.views.content.NavListView;
import com.example.shared.dto.TaskDTO;
import com.example.shared.services.UserService;
import com.example.shared.services.UserServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class NavListPresenter {
    private final NavListView view;
    private final UserServiceAsync userService = GWT.create(UserService.class);

    public NavListPresenter(NavListView view) {
        this.view = view;

        refreshView();
    }

    public NavListView getView() {
        return view;
    }

    private void bind() {
        if (SessionDataManager.getUser() == null) {
            view.logInBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    PresentersManager.getMainContentPresenter().toggleView(SessionDataManager.MainContentView.LOGIN);
                }
            });

            view.registerBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    PresentersManager.getMainContentPresenter().toggleView(SessionDataManager.MainContentView.REGISTER);
                }
            });
        } else {
            view.logOutBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    userService.logOut(new AsyncCallback<Void>() {
                        @Override
                        public void onSuccess(Void result) {
                            SessionDataManager.setUser(null);
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                            SessionDataManager.setUser(null);
                        }
                    });
                }
            });

            view.awaitingForUserBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    PresentersManager.getTaskContentPresenter()
                            .toggleView(TaskDTO.State.AWAITING_FOR_USER);
                }
            });

            view.inProgressBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    PresentersManager.getTaskContentPresenter()
                            .toggleView(TaskDTO.State.IN_PROGRESS);
                }
            });

            view.awaitingForAdminBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    PresentersManager.getTaskContentPresenter()
                            .toggleView(TaskDTO.State.AWAITING_FOR_ADMIN);
                }
            });

            view.finishedBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    PresentersManager.getTaskContentPresenter()
                            .toggleView(TaskDTO.State.FINISHED);
                }
            });
        }
    }

    public void refreshView() {
        view.refresh();
        bind();
    }
}
