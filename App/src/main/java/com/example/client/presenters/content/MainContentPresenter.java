package com.example.client.presenters.content;

import com.example.client.SessionDataManager;
import com.example.client.presenters.PresentersManager;
import com.example.client.views.content.MainContentView;

public class MainContentPresenter {
    private final MainContentView view;

    public MainContentPresenter(MainContentView view) {
        this.view = view;

        refreshView();
        bind();
    }

    public void bind() {

    }

    public void refreshView() {
        view.refresh(SessionDataManager.getMainContentView());
        if (SessionDataManager.getMainContentView() == SessionDataManager.MainContentView.TASKS) {
            PresentersManager.getTaskContentPresenter().refreshView();
        }
    }

    public MainContentView getView() {
        return view;
    }

    public void toggleView(SessionDataManager.MainContentView view) {
        if (SessionDataManager.getMainContentView() == view) {
            return;
        }

        SessionDataManager.setMainContentView(view);
        refreshView();
    }
}
