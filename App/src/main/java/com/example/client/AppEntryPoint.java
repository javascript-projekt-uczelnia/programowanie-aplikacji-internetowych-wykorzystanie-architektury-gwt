package com.example.client;

import com.example.client.presenters.PresentersManager;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

public class AppEntryPoint implements EntryPoint {

	@Override
	public void onModuleLoad() {
		SessionDataManager.init();

		RootPanel.getBodyElement().getStyle().setProperty("height", "100vh");
		RootPanel.getBodyElement().addClassName("columns p-0 m-0 has-background-black-bis");

		PresentersManager.init();

		RootPanel.get().add(PresentersManager.getNavListPresenter().getView());
		RootPanel.get().add(PresentersManager.getMainContentPresenter().getView());
	}
}
