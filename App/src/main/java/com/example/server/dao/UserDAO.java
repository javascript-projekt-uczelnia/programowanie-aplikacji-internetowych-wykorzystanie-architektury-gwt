package com.example.server.dao;

import com.example.server.model.User;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceException;
import jakarta.persistence.TypedQuery;

public class UserDAO {
    private final EntityManagerFactory entityManagerFactory;

    public UserDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void saveOrUpdate(User user) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.merge(user);
            entityManager.getTransaction().commit();
        } catch (PersistenceException e) {
            entityManager.getTransaction().rollback();
            throw new IllegalArgumentException("Login jest już używany.");
        } finally {
            entityManager.close();
        }
    }

    public User getById(Long id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User user = entityManager.find(User.class, id);
        entityManager.close();
        return user;
    }

    public boolean isUserCreated(User user) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            String queryString = "SELECT COUNT(u) FROM User u WHERE u.login = :login";
            TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
            query.setParameter("login", user.getLogin());

            Long count = query.getSingleResult();
            return count > 0;
        } catch (NoResultException e) {
            return false;
        } finally {
            entityManager.close();
        }
    }

    public User getUser(String login, String password) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            String queryString = "SELECT u FROM User u WHERE u.login = :login AND u.password = :password";
            TypedQuery<User> query = entityManager.createQuery(queryString, User.class);
            query.setParameter("login", login);
            query.setParameter("password", password);

            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }
}