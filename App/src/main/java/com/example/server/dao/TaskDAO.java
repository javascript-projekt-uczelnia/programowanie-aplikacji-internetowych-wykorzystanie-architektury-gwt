package com.example.server.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.PersistenceException;
import jakarta.persistence.TypedQuery;

import java.util.Date;
import java.util.List;

import com.example.server.model.Task;
import com.example.server.model.User;
import com.example.shared.dto.TaskDTO;

public class TaskDAO {
    private final EntityManagerFactory entityManagerFactory;

    public TaskDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void saveOrUpdate(Task task) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            if (task.getId() == null) {
                entityManager.persist(task);
            } else {
                entityManager.merge(task);
            }
            entityManager.getTransaction().commit();
        } catch (PersistenceException e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    public Task createTask(String title, String description, Date deadlineDate, User user) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Task task = new Task(title, description, new Date(), deadlineDate,
                    TaskDTO.State.AWAITING_FOR_USER.ordinal(), user);
            entityManager.persist(task);
            transaction.commit();
            return task;
        } catch (Exception ex) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw ex;
        } finally {
            entityManager.close();
        }
    }

    public List<Task> getAllTasks() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            TypedQuery<Task> query = entityManager.createQuery("SELECT t FROM Task t", Task.class);
            return query.getResultList();
        } finally {
            entityManager.close();
        }
    }

    public Task getById(Long id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Task task = entityManager.find(Task.class, id);
        entityManager.close();
        return task;
    }
}
