package com.example.server.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;

import java.util.Date;
import java.util.List;

import com.example.server.model.Task;
import com.example.server.model.TasksComment;
import com.example.server.model.User;

public class TasksCommentDAO {
    private final EntityManagerFactory entityManagerFactory;

    public TasksCommentDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public TasksComment createComment(User user, Task task, String content) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            TasksComment comment = new TasksComment(new Date(), user, content, task);
            entityManager.persist(comment);
            transaction.commit();
            return comment;
        } catch (Exception ex) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw ex;
        } finally {
            entityManager.close();
        }
    }

    public List<TasksComment> getComments(long taskId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            Task task = entityManager.find(Task.class, taskId);
            if (task == null) {
                return null;
            }
            TypedQuery<TasksComment> query = entityManager.createQuery(
                    "SELECT c FROM TasksComment c WHERE c.task = :task ORDER BY c.creationDate",
                    TasksComment.class);
            query.setParameter("task", task);
            return query.getResultList();
        } finally {
            entityManager.close();
        }
    }

}