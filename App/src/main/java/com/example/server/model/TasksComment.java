package com.example.server.model;

import jakarta.persistence.*;
import java.util.Date;

import com.example.shared.dto.TasksCommentDTO;

@Entity
public class TasksComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Date creationDate;

    @ManyToOne
    private User user;

    @Column(length = 1000, nullable = false)
    private String content;

    @ManyToOne
    private Task task;

    public TasksComment() {
    }

    public TasksComment(Date creationDate, User user, String content, Task task) {
        this.creationDate = creationDate;
        this.user = user;
        this.content = content;
        this.task = task;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public TasksCommentDTO getDTO() {
        return new TasksCommentDTO(id, creationDate, user.getDTO(), content, task.getDTO());
    }
}
