package com.example.server.model;

import jakarta.persistence.*;
import java.util.Date;
import com.example.shared.dto.TaskDTO;
import com.example.shared.dto.UserDTO;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, nullable = false)
    private String title;

    @Column(length = 1000, nullable = false)
    private String description;

    private Date creationDate;

    private Date deadlineDate;

    @Column(nullable = false)
    private int state;

    @ManyToOne
    private User creator;

    @ManyToOne
    private User assignee;

    public Task() {
    }

    public Task(String title, String description, Date creationDate, Date deadlineDate,
            int state, User creator) {
        this.title = title;
        this.description = description;
        this.creationDate = creationDate;
        this.deadlineDate = deadlineDate;
        this.state = state;
        this.creator = creator;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(Date deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public TaskDTO getDTO() {
        UserDTO assigneeDTO = assignee == null ? null : assignee.getDTO();
        return new TaskDTO(getId(), getTitle(), getDescription(), getCreationDate(),
                getDeadlineDate(), TaskDTO.State.values()[getState()], getCreator().getDTO(),
                assigneeDTO);
    }
}