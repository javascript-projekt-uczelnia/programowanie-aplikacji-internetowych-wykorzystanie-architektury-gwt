package com.example.server.services;

import com.example.shared.services.CommentService;
import com.example.server.HibernateUtil;
import com.example.server.model.Task;
import com.example.server.model.TasksComment;
import com.example.server.model.User;
import com.example.shared.dto.TasksCommentDTO;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

@WebServlet("/commentService")
public class CommentServiceImpl extends RemoteServiceServlet implements CommentService {

    private User getSessionUser() {
        HttpSession session = getThreadLocalRequest().getSession();
        Object object = session.getAttribute("user");

        return object == null ? null : (User) object;
    }

    @Override
    public List<TasksCommentDTO> getAllComments(long taskId) {
        List<TasksCommentDTO> list = new ArrayList<>();
        List<TasksComment> comments = HibernateUtil.getTasksCommentDAO().getComments(taskId);

        for (TasksComment comment : comments) {
            list.add(comment.getDTO());
        }

        return list;
    }

    @Override
    public List<TasksCommentDTO> createComment(long taskId, String content) {
        User user = getSessionUser();
        if (user == null) {
            return null;
        }

        Task task = HibernateUtil.getTaskDAO().getById(taskId);
        if (task == null) {
            return null;
        }

        HibernateUtil.getTasksCommentDAO().createComment(user, task, content);
        return getAllComments(taskId);
    }
}