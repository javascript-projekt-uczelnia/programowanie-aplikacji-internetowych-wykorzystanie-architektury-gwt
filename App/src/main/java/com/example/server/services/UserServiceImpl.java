package com.example.server.services;

import com.example.server.HibernateUtil;
import com.example.server.dao.UserDAO;
import com.example.server.model.User;
import com.example.shared.dto.UserDTO;
import com.example.shared.services.UserService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

@WebServlet("/userService")
public class UserServiceImpl extends RemoteServiceServlet implements UserService {
    private User getSessionUser() {
        HttpSession session = getThreadLocalRequest().getSession();
        Object object = session.getAttribute("user");

        return object == null ? null : (User) object;
    }

    private void setSessionUser(User user) {
        HttpSession session = getThreadLocalRequest().getSession();
        if (user != null) {
            session.setAttribute("user", user);
        } else {
            session.removeAttribute("user");
        }
    }

    @Override
    public UserDTO whoAmI() {
        User user = getSessionUser();
        return user != null ? user.getDTO() : null;
    }

    @Override
    public UserDTO logIn(String login, String password) {
        User user = HibernateUtil.getUserDAO().getUser(login, password);
        if (user != null) {
            setSessionUser(user);
            return user.getDTO();
        } else {
            return null;
        }
    }

    @Override
    public Boolean registerIn(String login, String password) {
        UserDAO userDAO = HibernateUtil.getUserDAO();

        User user = new User(login, password);
        if (userDAO.isUserCreated(user)) {
            return false;
        }

        userDAO.saveOrUpdate(user);
        setSessionUser(user);
        return true;
    }

    @Override
    public void logOut() {
        setSessionUser(null);
    }
}
