package com.example.server.services;

import com.example.server.HibernateUtil;
import com.example.server.dao.TaskDAO;
import com.example.server.model.Task;
import com.example.shared.services.TaskService;
import com.example.shared.dto.TaskDTO;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.example.server.model.User;

@WebServlet("/taskService")

public class TaskServiceImpl extends RemoteServiceServlet implements TaskService {
    private User getSessionUser() {
        HttpSession session = getThreadLocalRequest().getSession();
        Object object = session.getAttribute("user");

        return object == null ? null : (User) object;
    }

    @Override
    public List<TaskDTO> getAllTasks() {

        List<TaskDTO> list = new ArrayList<>();
        List<Task> tasks = HibernateUtil.getTaskDAO().getAllTasks();

        for (Task task : tasks) {
            list.add(task.getDTO());
        }

        return list;
    }

    @Override
    public List<TaskDTO> createTask(String title, String description, Date date) {
        User user = getSessionUser();
        if (user == null) {
            return null;
        }

        if (!user.isAdmin()) {
            return null;
        }

        HibernateUtil.getTaskDAO().createTask(title, description, date,
                (User) user);

        return getAllTasks();
    }

    @Override
    public List<TaskDTO> setAssignee(Long taskId) {
        User user = getSessionUser();
        if (user == null) {
            return null;
        }

        TaskDAO taskDAO = HibernateUtil.getTaskDAO();
        Task task = taskDAO.getById(taskId);

        if (task == null) {
            return null;
        }

        User curUser = task.getAssignee();
        if (curUser == null) {
            task.setAssignee(user);
            task.setState(TaskDTO.State.IN_PROGRESS.ordinal());
            taskDAO.saveOrUpdate(task);
        } else {
            if ((curUser.getId() == user.getId()) || user.isAdmin()) {
                task.setAssignee(null);
                task.setState(TaskDTO.State.AWAITING_FOR_USER.ordinal());
                taskDAO.saveOrUpdate(task);
            } else {
                return null;
            }
        }

        return getAllTasks();
    }

    @Override
    public List<TaskDTO> setState(Long taskId, int state) {
        User user = getSessionUser();
        if (user == null) {
            return null;
        }

        TaskDAO taskDAO = HibernateUtil.getTaskDAO();
        Task task = taskDAO.getById(taskId);

        if (task == null) {
            return null;
        }

        User curUser = task.getAssignee();
        if (user.isAdmin()) {
            task.setState(state);
            taskDAO.saveOrUpdate(task);
        } else if (curUser.getId() == user.getId()) {
            if (state != TaskDTO.State.FINISHED.getValue()) {
                task.setState(state);
                taskDAO.saveOrUpdate(task);
            } else {
                return null;
            }
        } else {
            return null;
        }

        return getAllTasks();
    }
}