package com.example.server;

import com.example.server.dao.TaskDAO;
import com.example.server.dao.TasksCommentDAO;
import com.example.server.dao.UserDAO;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class HibernateUtil {
    private static Boolean isInicialized = false;
    private static UserDAO userDAO;
    private static TaskDAO taskDAO;
    private static TasksCommentDAO tasksCommentDAO;

    private static void init() {
        if (!isInicialized) {
            try {
                EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MyPersistence");
                userDAO = new UserDAO(entityManagerFactory);
                taskDAO = new TaskDAO(entityManagerFactory);
                tasksCommentDAO = new TasksCommentDAO(entityManagerFactory);
                isInicialized = true;
            } catch (Throwable ex) {
                throw new ExceptionInInitializerError(ex);
            }
        }
    }

    public static UserDAO getUserDAO() {
        init();
        return userDAO;
    }

    public static TaskDAO getTaskDAO() {
        init();
        return taskDAO;
    }

    public static TasksCommentDAO getTasksCommentDAO() {
        init();
        return tasksCommentDAO;
    }
}
