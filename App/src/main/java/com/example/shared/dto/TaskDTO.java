package com.example.shared.dto;

import java.io.Serializable;
import java.util.Date;

public class TaskDTO implements Serializable {
    public static enum State {
        AWAITING_FOR_USER(0),
        IN_PROGRESS(1),
        AWAITING_FOR_ADMIN(2),
        FINISHED(3);

        private final int value;

        State(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private Long id;
    private String title;
    private String description;
    private Date creationDate;
    private Date deadlineDate;
    private State state;
    private UserDTO creator;
    private UserDTO assignee;

    public TaskDTO() {
    }

    public TaskDTO(Long id, String title, String description, Date creationDate,
            Date deadlineDate, State state, UserDTO creator, UserDTO assignee) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.creationDate = creationDate;
        this.deadlineDate = deadlineDate;
        this.state = state;
        this.creator = creator;
        this.assignee = assignee;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getDeadlineDate() {
        return deadlineDate;
    }

    public State getState() {
        return state;
    }

    public UserDTO getCreator() {
        return creator;
    }

    public UserDTO getAssignee() {
        return assignee;
    }
}