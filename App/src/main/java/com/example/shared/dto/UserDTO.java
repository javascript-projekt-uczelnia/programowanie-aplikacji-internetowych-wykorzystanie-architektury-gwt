package com.example.shared.dto;

import java.io.Serializable;

public class UserDTO implements Serializable {
    private String login;
    private boolean isAdmin;

    public UserDTO() {
    }

    public UserDTO(String login, boolean isAdmin) {
        this.login = login;
        this.isAdmin = isAdmin;
    }

    public String getLogin() {
        return login;
    }

    public boolean isAdmin() {
        return isAdmin;
    }
}
