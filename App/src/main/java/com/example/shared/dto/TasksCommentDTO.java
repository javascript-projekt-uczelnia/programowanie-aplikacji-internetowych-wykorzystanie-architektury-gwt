package com.example.shared.dto;

import java.io.Serializable;
import java.util.Date;

public class TasksCommentDTO implements Serializable {
    private Long id;
    private Date creationDate;
    private UserDTO user;
    private String content;
    private TaskDTO task;

    public TasksCommentDTO() {
    }

    public TasksCommentDTO(Long id, Date creationDate, UserDTO user, String content, TaskDTO task) {
        this.id = id;
        this.creationDate = creationDate;
        this.user = user;
        this.content = content;
        this.task = task;
    }

    public Long getId() {
        return id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public UserDTO getUser() {
        return user;
    }

    public String getContent() {
        return content;
    }

    public TaskDTO getTask() {
        return task;
    }
}
