package com.example.shared.services;

import com.example.shared.dto.UserDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("userService")
public interface UserService extends RemoteService {
    UserDTO whoAmI();

    UserDTO logIn(String login, String password);

    Boolean registerIn(String login, String password);

    void logOut();
}