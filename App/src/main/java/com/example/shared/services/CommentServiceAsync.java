package com.example.shared.services;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import com.example.shared.dto.TasksCommentDTO;

public interface CommentServiceAsync {
    void getAllComments(long taskId, AsyncCallback<List<TasksCommentDTO>> callback);

    void createComment(long taskId, String content, AsyncCallback<List<TasksCommentDTO>> callback);
}