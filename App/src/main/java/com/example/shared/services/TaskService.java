package com.example.shared.services;

import java.util.Date;
import java.util.List;
import com.example.shared.dto.TaskDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("taskService")
public interface TaskService extends RemoteService {
    List<TaskDTO> getAllTasks();

    List<TaskDTO> createTask(String title, String description, Date date);

    List<TaskDTO> setAssignee(Long taskId);

    List<TaskDTO> setState(Long taskId, int state);
}