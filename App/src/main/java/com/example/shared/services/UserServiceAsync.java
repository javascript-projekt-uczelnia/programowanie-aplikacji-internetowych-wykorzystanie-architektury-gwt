package com.example.shared.services;

import com.example.shared.dto.UserDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface UserServiceAsync {
    void whoAmI(AsyncCallback<UserDTO> asyncCallback);

    void logIn(String login, String password, AsyncCallback<UserDTO> asyncCallback);

    void registerIn(String login, String password, AsyncCallback<Boolean> asyncCallback);

    void logOut(AsyncCallback<Void> asyncCallback);
}