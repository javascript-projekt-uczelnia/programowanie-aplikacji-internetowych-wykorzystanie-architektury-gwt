package com.example.shared.services;

import java.util.Date;
import java.util.List;
import com.example.shared.dto.TaskDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TaskServiceAsync {
    void getAllTasks(AsyncCallback<List<TaskDTO>> asyncCallback);

    void createTask(String title, String description, Date date,
            AsyncCallback<List<TaskDTO>> asyncCallback);
            
    void setAssignee(Long taskId, AsyncCallback<List<TaskDTO>> asyncCallback);

    void setState(Long taskId, int state, AsyncCallback<List<TaskDTO>> asyncCallback);
}