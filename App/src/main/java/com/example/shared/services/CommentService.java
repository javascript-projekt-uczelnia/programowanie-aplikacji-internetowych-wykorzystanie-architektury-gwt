package com.example.shared.services;

import java.util.List;
import com.example.shared.dto.TasksCommentDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("commentService")
public interface CommentService extends RemoteService {
    List<TasksCommentDTO> getAllComments(long taskId);

    List<TasksCommentDTO> createComment(long taskId, String content);
}