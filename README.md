# Your GWT App

## Run GWT devmode

```
mvn clean install
mvn gwt:generate-module gwt:devmode
```

Tutaj wnioski, które potem jakoś ładnie opiszę

- aplikacja zapisuje swój stan poprzez SessionDataStorage, przez co odświeżenie aplikacji nie powinno zmienić zawartości, która jest wyświetlana użytkownikowi,
- obsługa sesji po stronie serwera, wcześniej niektóre dane trzymałem w SessionStorage, co było poważnym zagrożeniem dla bezpieczeństwa,
- zapisywanie danych po stronie clienta w SessionStorage, by przeglądarka zapamiętywała stan aplikacji,
